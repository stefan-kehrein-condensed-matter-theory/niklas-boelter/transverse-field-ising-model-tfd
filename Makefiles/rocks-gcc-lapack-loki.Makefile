CC=gcc
CFLAGS=-O2 -march=skylake-avx512 -Wall -I/home/niklas/static-libs/include -static
LDFLAGS=-lm -lsqlite3 -lgmp -lbsd -L/home/niklas.boelter/lib -llapack -lblas -lpthread -ldl -L /home/niklas/static-libs/lib -L /home/niklas/static-libs/lib64 -static-libgfortran -static-libgcc -lm -lgfortran -lquadmath -lm

include Makefile.in
