CC=gcc
CFLAGS=-O2 -std=gnu99 -pedantic -Wall -isystem/usr/local/opt/lapack/include -isystem/usr/include/lapacke/ -DUSE_BMI2_FALLBACK -I/home/niklas.boelter/include
LDFLAGS=-lm -llapack -lsqlite3 -L/usr/local/opt/lapack/lib -lgmp -lbsd -L/home/niklas.boelter/lib -Wl,-rpath,/home/niklas.boelter/lib

include Makefile.in
