MKLROOT=/share/opt/intel/compilers_and_libraries_2018.0.128/linux/mkl
CC=gcc
CFLAGS=-O2 -march=native -Wall -DUSE_INTEL_MKL -isystem$(MKLROOT)/include/ -isystem/home/niklas.boelter/include -mbmi2
LDFLAGS=-L/home/niklas.boelter/lib -L/home/niklas.boelter/lib64 -L$(MKLROOT)/lib/intel64_lin -lm -lsqlite3 -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lgmp -lbsd -Wl,-rpath,/home/niklas.boelter/lib -Wl,-rpath,/home/niklas.boelter/lib64 -Wl,-rpath,$(MKLROOT)/lib/intel64_lin

include Makefile.in
