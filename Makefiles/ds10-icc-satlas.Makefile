CC=icc
CFLAGS=-O2 -xHost -Wall -I /usr/include/lapacke
LDFLAGS=-L/home/niklas.boelter/lib -L/usr/lib64 -lm -lsqlite3 -llapack /usr/lib64/libopenblas64_-r0.3.3.so -lgmp -lbsd -Wl,-rpath,/home/niklas.boelter/lib -Wl,-rpath,/usr/lib64

include Makefile.in
