CC=icc -static-intel -wd10237
CFLAGS=-O2 -xCOMMON-AVX512 -Wall -mkl=sequential -DUSE_INTEL_MKL -I/net/theorie/rocks/niklas.boelter/sqlite-latest 
LDFLAGS=-lsqlite3 -L/net/theorie/rocks/niklas.boelter/sqlite/lib/ -lgmp -lbsd

include Makefile.in
