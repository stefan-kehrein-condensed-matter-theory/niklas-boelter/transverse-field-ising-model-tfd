CC=gcc -DUSE_BMI2_FALLBACK
CFLAGS=-O3 -Wall -Wno-missing-variable-declarations -Wno-sign-conversion -Wno-reserved-id-macro -Wno-strict-prototypes -Wno-bad-function-cast -Wno-double-promotion -I/usr/local/opt/lapack/include -I/usr/include/lapacke/ -mbmi2
LDFLAGS=-lm -llapack -lsqlite3 -L/usr/local/opt/lapack/lib -lgmp -lbsd

include Makefile.in
