MKLROOT=/opt/intel/mkl
CC=clang -DUSE_INTEL_MKL
CFLAGS=-O0 -fsanitize=address -g -march=native -Wall -DUSE_INTEL_MKL -I$(MKLROOT)/include/ -mbmi2
LDFLAGS=-lsqlite3 -lm -lmkl_core -lmkl_intel_lp64 -lmkl_sequential -L$(MKLROOT)/lib/intel64_lin/ -lgmp -lbsd -Wl,-rpath,$(MKLROOT)/lib/intel64_lin/

include Makefile.in
