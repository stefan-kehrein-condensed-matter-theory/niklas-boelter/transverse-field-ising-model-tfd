MKLROOT=/opt/intel/mkl
CC=icc
CFLAGS=-std=c11 -wd10237 -O2 -xHost -Wall -Wremarks -Wcheck -DUSE_INTEL_MKL -I$(MKLROOT)/include 
LDFLAGS=-lsqlite3 -lgmp -lbsd -L$(MKLROOT)/lib/intel64_lin/ -Wl,-rpath,$(MKLROOT)/lib/intel64_lin/ -lmkl_intel_lp64 -lmkl_sequential -lmkl_core 

include Makefile.in
