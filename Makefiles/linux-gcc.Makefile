CC=gcc
CFLAGS=-O2 -march=native -Wall -mbmi2
LDFLAGS=-lsqlite3 -lm -llapack -lgmp -lbsd

include Makefile.in
