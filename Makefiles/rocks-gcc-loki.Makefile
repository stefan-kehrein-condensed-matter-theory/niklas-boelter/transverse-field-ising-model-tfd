MKLROOT=/opt/intel/mkl
#/share/opt/intel/compilers_and_libraries_2018.0.128/linux/mkl
CC=gcc
CFLAGS=-O2 -march=skylake-avx512 -Wall -DUSE_INTEL_MKL -I/home/niklas.boelter/include -I$(MKLROOT)/include -static
LDFLAGS=-lm -lsqlite3 -lgmp -lbsd -L/home/niklas.boelter/lib  -Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_sequential.a $(MKLROOT)/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm -ldl -L /home/niklas/static-libs/lib

include Makefile.in
