MKLROOT=/share/opt/intel/compilers_and_libraries_2018.0.128/linux/mkl
CC=icc
CFLAGS=-O2 -xHost -Wall -DUSE_INTEL_MKL -I/home/niklas.boelter/include -I$(MKLROOT)/include
LDFLAGS=-L/home/niklas.boelter/lib -L$(MKLROOT)/lib/intel64_lin -lm -lsqlite3 -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lgmp -lbsd -Wl,-rpath,/home/niklas.boelter/lib -Wl,-rpath,$(MKLROOT)/lib/intel64_lin

include Makefile.in
