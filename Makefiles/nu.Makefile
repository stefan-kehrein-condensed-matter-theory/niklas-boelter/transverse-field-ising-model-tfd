CC=clang
CFLAGS=-O2 -std=c99 -Weverything -mbmi2 -mpopcnt -march=native -isystem/usr/local/opt/gmp/include -isystem/usr/local/opt/lapack/include -isystem/usr/local/include
LDFLAGS=-lm -llapack -lsqlite3 -L/usr/local/opt/lapack/lib -lgmp -L/usr/local/opt/gmp/lib

include Makefile.in
