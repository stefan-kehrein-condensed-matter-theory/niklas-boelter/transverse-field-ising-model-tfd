#!/usr/bin/env fish
set code_version (git describe --always)

set tablename results_{$code_version}_dirty
echo "DROP TABLE IF EXISTS $tablename;" | sqlite3 FC-TFIM-QUENCH.db

set tablename results_Haar_{$code_version}_dirty
echo "DROP TABLE IF EXISTS $tablename;" | sqlite3 FC-TFIM-QUENCH.db

set tablename results_equilibrium_{$code_version}_dirty
echo "DROP TABLE IF EXISTS $tablename;" | sqlite3 FC-TFIM-QUENCH.db
