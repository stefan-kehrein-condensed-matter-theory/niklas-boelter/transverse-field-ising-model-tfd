/*  Copyright 2019 Niklas Bölter <niklas.boelter@theorie.physik.uni-goettingen.de> */
#include "util.h"

#define TABLENAME "results_equilibrium_" GIT_DESCRIBE

#include <assert.h>
#include <sqlite3.h>

#define DBPATH "FC-TFIM-QUENCH.db"

// System Parameters
static int N = 60;
static double beta = 0;
static double Gamma = 0.25;
static double lf = 0.0;

// Observables
static double magnetization_mean;
static double magnetization_variance;
static double magnetization_skewness;
static double magnetization_kurtosis;

static double magnetization_A_mean;
static double magnetization_A_variance;
static double magnetization_A_skewness;
static double magnetization_A_kurtosis;

static double S_AB, S2_AB, Smin_AB;
static double S_A, S2_A, Smin_A;
static double energy;

static int spins_A, spins_B;
static int dim_A, dim_B;


static double *hamiltonian;
static double *eigenvalues;

static double *rho_AB;
static double *rho_AB_eigenvalues;

static double *rho_A;
static double *rho_A_eigenvalues;

static int find_results(void);
static void store_results(void);
static void usage(char* argv0);

void usage(char *argv0)
{
    printf("Usage: %s N gamma beta lf\n\n", argv0);
    printf("Calculates equilibrium properties\n");
    printf("System: fully connected TFIM partition\n\n");
    printf("N\t Number of spins, should be even\n");
    printf("Gamma\t coupling strength\n");
    printf("beta\t Inverse temperature\n");
    printf("lf\t Strength of longitudinal field\n");

#ifdef USE_INTEL_MKL
    MKLVersion Version;
 
    mkl_get_version(&Version);
    printf("\n\n"); 
    printf("MKL version %d.%d.%d build %s\n", Version.MajorVersion, Version.MinorVersion, Version.UpdateVersion, Version.Build);
    printf("%s\n", Version.Processor); 
#endif
    
    printf("\n\nVersion %s (compiler version %s)\n", GIT_DESCRIBE, __VERSION__);
}

int main(int argc, char *argv[])
{
    clock_t init;
    char buf[1024];
    char buf2[1024];
    long long i,j,k;

    long long dim;

    double *binomial_prefactors_AB;

#if defined(USE_INTEL_MKL) && defined(DEBUG)
    mkl_verbose(1);
#endif


#ifdef USE_INTEL_MKL
    if(mkl_enable_instructions(MKL_ENABLE_AVX2) != 1)
    {
        fprintf(stderr, "WARNING: mkl_enable_instructions(MKL_ENABLE_AVX2) was rejected by MKL\n");
    }
    mkl_verbose(1);
#endif // ifdef USE_INTEL_MKL

    
    init = clock();

    if(argc != 5)
    {
        usage(argv[0]);
        exit(EX_USAGE);
    }

    N = atoi(argv[1]);
    Gamma = atof(argv[2]);
    beta = atof(argv[3]);
    lf = atof(argv[4]);

    spins_A = spins_B = N/2;

    dim = N+1;
    dim_A = spins_A+1;
    dim_B = spins_B+1;


    if(N < 1 || N%2 != 0)
    {
        fprintf(stderr, "Parameter N should be positive and even.");
        exit(EX_USAGE);
    }
    if(beta < 0.0)
    {
        fprintf(stderr, "The temperature must be positive.");
        exit(EX_USAGE);
    }

    argv_len = 0;
    for(i = 0; i < argc; i++)
    {
        argv_len += strlen(argv[i]) + 1;
    }
    argv_ptr = &argv[0][0];
    buf[0] = '\0';
    status_prefix = buf;

    if(find_results())
    {
        printf("Skipping.\n");
        exit(0);
    }

    double required_memory = 0.0;
    required_memory += 2 * dim * dim;
    required_memory += 2 * dim;
    required_memory += 2 * dim_A * dim_A;
    required_memory += dim_A;
    required_memory = required_memory * (double) sizeof(double);

    printf("Version %s (compiler version %s)\n", GIT_DESCRIBE, __VERSION__);
    printf("I will need %i MiB of heap memory.\n", (int) (required_memory/(1024*1024)+1));
    report_status("Initializing ");
    
    hamiltonian = my_calloc(dim * dim, sizeof(double));
    eigenvalues = my_calloc( dim, sizeof(double));

    rho_AB = my_calloc(dim * dim, sizeof(double));
    rho_AB_eigenvalues = my_calloc(dim, sizeof(double));

    rho_A = my_calloc(dim_A * dim_A, sizeof(double));
    rho_A_eigenvalues = my_calloc(dim_A, sizeof(double));

    binomial_prefactors_AB = my_calloc(dim_A * dim_A, sizeof(double));


    if(!hamiltonian || !eigenvalues || !rho_AB || !rho_AB_eigenvalues 
        || !rho_A || !rho_A_eigenvalues)
    {
        fprintf(stderr, "ERROR: Could not allocate memory.\n");
        exit(EX_OSERR);
    }

    for(int spinsUp_A = 0; spinsUp_A <= spins_A; spinsUp_A++)
    {
        for(int spinsUp_B = 0; spinsUp_B <= spins_B; spinsUp_B++)
        {
            binomial_prefactors_AB[spinsUp_A + dim_A * spinsUp_B] = 
                binomial_prefactor(spins_A, spins_B, spinsUp_A, spinsUp_B);
        }
    }


    // Construct Hamiltonian
    for(int spinsUp = 0; spinsUp < dim; spinsUp++)
    {
        double s = (double) spinsUp / (double) N - 0.5;
        hamiltonian[spinsUp + dim * spinsUp] = -0.5 * s * s - lf * s/N;
        
        if(spinsUp < N)
        {
            hamiltonian[(spinsUp+1) + dim * spinsUp] = -0.5 * Gamma 
                * sqrt(0.25 - s*s + (0.5-s)/N);
        }

        if(spinsUp > 0)
        {
            hamiltonian[(spinsUp-1) + dim * spinsUp] = -0.5 * Gamma 
                * sqrt(0.25 - s*s + (0.5+s)/N);
        }
    }

#ifdef DEBUG
    check_symmetric(hamiltonian, dim);
#endif // ifdef DEBUG
    report_elapsed_time();

    start = clock();
    
    snprintf(buf2, sizeof(buf2), "Diagonalizing Hamiltonian (%lli x %lli) ", dim, dim);
    report_status(buf2);
    diagonalize_symmetric_eigenvectors(hamiltonian, (lapack_int) dim, eigenvalues);

    start = clock();
    report_status("Calculating rho_AB ");

    double min_eigenvalue = 0.0;

    // printf("\n");

    for(k = 0; k < dim; k++)
    {
        if(eigenvalues[k] < min_eigenvalue)
        {
            min_eigenvalue = eigenvalues[k];
        }
        // if(k != 0)
        // {
        //     printf(", ");
        // }
        // printf("%lf", eigenvalues[k]);
    }

    // printf("\n");

    double partitionSum = 0.0;
    for(k = 0; k < dim; k++)
    {
        partitionSum += exp(-1.0 * beta * N * (eigenvalues[k] - min_eigenvalue));
    }

    for(k = 0; k < dim; k++)
    {
        for(i = 0; i < dim; i++)
        {
            for(j = 0; j < dim; j++)
            {
                rho_AB[i + dim * j] +=
                    exp(-1.0 * beta * N * (eigenvalues[k] - min_eigenvalue))
                    * hamiltonian[i + dim * k]
                    * hamiltonian[j + dim * k]
                    / partitionSum;
            }
        }
    }

#ifdef DEBUG
    check_symmetric(rho_AB, dim);
#endif // ifdef DEBUG
    report_elapsed_time();


    double magnetization_sum = 0.0;
    energy = 0.0;
    for(i = 0; i < dim; i++)
    {
        double s = (2 * i - N);
        energy += eigenvalues[i] * exp(-1.0 * beta * N * (eigenvalues[i] - min_eigenvalue));
        magnetization_sum += + s * rho_AB[i + dim * i];
    }
    energy = energy/partitionSum;

    double magnetization_first_central_moment = 0.0;
    double magnetization_second_central_moment = 0.0;
    double magnetization_third_central_moment = 0.0;
    double magnetization_fourth_central_moment = 0.0;

    for(i = 0; i < dim; i++)
    {
        double s = (2 * i - N) - magnetization_sum;

        magnetization_first_central_moment += pow(s, 1.0) * rho_AB[i + dim * i];;
        magnetization_second_central_moment += pow(s, 2.0) * rho_AB[i + dim * i];;
        magnetization_third_central_moment += pow(s, 3.0) * rho_AB[i + dim * i];;
        magnetization_fourth_central_moment += pow(s, 4.0) * rho_AB[i + dim * i];;

    }

    magnetization_mean = magnetization_sum / (2 * N);
    magnetization_variance = magnetization_second_central_moment / (4 * N * N);
    magnetization_skewness = 
        magnetization_third_central_moment/pow(magnetization_second_central_moment, 1.5);
    magnetization_kurtosis = 
        magnetization_fourth_central_moment/pow(magnetization_second_central_moment, 2.0);


    for(int A_row = 0; A_row <= spins_A; A_row++)
    {
        for(int A_column = 0; A_column <= spins_A; A_column++)
        {
            for(int B = 0; B <= spins_B; B++)
            {
                rho_A[A_row + dim_A * A_column] += 
                    binomial_prefactors_AB[A_row + dim_A*B] 
                    * binomial_prefactors_AB[A_column + dim_A*B] 
                    * rho_AB[(A_row+B) + dim * (A_column + B)];
            }
        }
    }

        // Magnetization of subsystem C
    double magnetization_A_sum = 0.0;

    // printf("\nMagnetization Distribution beta = %lf\n[", beta);
    // double test_trace = 0.0;
    for(i = 0; i < dim_A; i++)
    {
        double s = (2 * i - N/2);
        magnetization_A_sum += s * rho_A[i + dim_A * i];

        // printf("%lf", rho_A[i + dim_A * i]);
        // test_trace += rho_A[i + dim_A * i];
    }
    // printf("],\n");


    printf("\nMagnetization Distribution beta = %lf\n[", beta);
    double test_trace = 0.0;
    for(i = 0; i < dim; i++)
    {
        double s = (2 * i - N);
        magnetization_A_sum += s * rho_AB[i + dim * i];

        printf("%lf", rho_AB[i + dim * i]);
        test_trace += rho_AB[i + dim * i];
        if(i+1 < dim)
        {
            printf(", ");
        }
    }
    printf("],\n");

    printf("Test Trace: %lf\n", test_trace);

    magnetization_A_mean = magnetization_A_sum / N;

    double magnetization_A_first_central_moment = 0.0;
    double magnetization_A_second_central_moment = 0.0;
    double magnetization_A_third_central_moment = 0.0;
    double magnetization_A_fourth_central_moment = 0.0;

    for(i = 0; i < dim_A; i++)
    {
        double s = (2 * i - N/2) - magnetization_A_sum;

        magnetization_A_first_central_moment += pow(s, 1.0) * rho_A[i + dim_A * i];
        magnetization_A_second_central_moment += pow(s, 2.0) * rho_A[i + dim_A * i];
        magnetization_A_third_central_moment += pow(s, 3.0) * rho_A[i + dim_A * i];
        magnetization_A_fourth_central_moment += pow(s, 4.0) * rho_A[i + dim_A * i];

    }

    magnetization_A_variance = magnetization_A_second_central_moment/(N*N);
    magnetization_A_skewness = magnetization_A_third_central_moment
        / pow(magnetization_A_second_central_moment, 1.5);
    magnetization_A_kurtosis = magnetization_A_fourth_central_moment
        / pow(magnetization_A_second_central_moment, 2.0);

    start = clock();
    snprintf(buf2, sizeof(buf2), "Diagonalizing rho_AB (%lli x %lli) ", dim, dim);
    report_status(buf2);
    diagonalize_symmetric(rho_AB, (lapack_int) dim, rho_AB_eigenvalues);

    S_AB = calculate_vonNeumann_entropy(rho_AB_eigenvalues, dim);
    S2_AB = calculate_Renyi_entropy(rho_AB_eigenvalues, dim, 2.0);
    Smin_AB = calculate_min_entropy(rho_AB_eigenvalues, dim);

    start = clock();
    snprintf(buf2, sizeof(buf2), "Diagonalizing rho_A (%i x %i) ", dim_A, dim_A);
    report_status(buf2);
    diagonalize_symmetric(rho_A, (lapack_int) dim_A, rho_A_eigenvalues);

    S_A = calculate_vonNeumann_entropy(rho_A_eigenvalues, dim_A);
    S2_A = calculate_Renyi_entropy(rho_A_eigenvalues, dim_A, 2.0);
    Smin_A = calculate_min_entropy(rho_A_eigenvalues, dim_A);

    printf("\n\n");
    printf("=====  Equilibrium, Γ = %lg, h = %lg =====\n\n", Gamma, lf);

    printf("\n");
    printf("                      Beta: %lg\n", beta);
    printf("            Energy Density: %lg\n", energy);
    printf("                      S_AB: %lg\n", S_AB);
    printf("                     S2_AB: %lg\n", S2_AB);
    printf("\n");
    printf("    Magnetization         : %lf ± %lf\n", 
        magnetization_mean, sqrt(magnetization_variance));
    printf("    Magnetization Variance: %lf\n", magnetization_variance);
    printf("    Magnetization Skewness: %lf\n", magnetization_skewness);
    printf("    Magnetization Kurtosis: %lf\n", magnetization_kurtosis);
    printf("\n\n");

    // printf("    Magnetization m_1: %lf\n", magnetization_first_central_moment);
    // printf("    Magnetization m_2: %lf\n", magnetization_second_central_moment);
    // printf("    Magnetization m_3: %lf\n", magnetization_third_central_moment);
    // printf("    Magnetization m_4: %lf\n", magnetization_fourth_central_moment);

    printf("=====  Subsystem A  =====\n\n");
    printf("    S_A  = %g\n", S_A);
    printf("    S2_A = %g\n", S2_A);
    printf("\n");

    printf("    Magnetization A         : %lf ± %lf\n", 
        magnetization_A_mean, sqrt(magnetization_A_variance));
    printf("    Magnetization A Variance: %lf\n", magnetization_A_variance);
    printf("    Magnetization A Skewness: %lf\n", magnetization_A_skewness);
    printf("    Magnetization A Kurtosis: %lf\n", magnetization_A_kurtosis);

    
    store_results();

#ifdef USE_INTEL_MKL
    mkl_free_buffers();
    mkl_finalize();
#endif // ifdef USE_INTEL_MKL


    free(rho_A);
    free(rho_A_eigenvalues);
    free(rho_AB);
    free(rho_AB_eigenvalues);
    free(binomial_prefactors_AB);
    free(eigenvalues);
    free(hamiltonian);
    
    start = init;

    printf("\n");
    report_elapsed_time();
    return 0;
}

int find_results(void)
{
    // Sqlite3 
    sqlite3 *db;
    int rc;
    char *zErrMsg = 0;
    sqlite3_stmt *stmt;
    int count;

    rc = sqlite3_open(DBPATH, &db);
    if(rc) {
        fprintf(stderr, "\nError: Can't open database in %s:%i: %s\n", __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    sqlite3_busy_timeout(db, 100000);

    rc = sqlite3_exec(db, 
        "CREATE TABLE IF NOT EXISTS " TABLENAME " "
        "(N INTEGER, gamma REAL, beta REAL, lf REAL, "
        "magnetization_mean REAL, magnetization_variance REAL, "
        "magnetization_skewness REAL, magnetization_kurtosis REAL, "
        "magnetization_A_mean REAL, magnetization_A_variance REAL, "
        "magnetization_A_skewness REAL, magnetization_A_kurtosis REAL, "
        "S_AB REAL, S2_AB REAL, Smin_AB REAL, "
        "S_A REAL, S2_A REAL, Smin_A REAL, "
        "energy REAL, version TEXT,"
        "CONSTRAINT " TABLENAME "_uniq UNIQUE "
        "(N, gamma, beta, lf))", NULL, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, zErrMsg);
        sqlite3_free(zErrMsg);
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_prepare_v2(db, "SELECT COUNT(*) FROM " TABLENAME " WHERE N = ?1 AND gamma = ?2 "
        "AND beta = ?3 AND lf = ?4;", -1, &stmt, NULL);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_int(stmt, 1, N);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 2, Gamma);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 3, beta);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 4, lf);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_step(stmt);
    if(rc != SQLITE_ROW)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }
    
    count = sqlite3_column_int(stmt, 0);

    rc = sqlite3_reset(stmt);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_finalize(stmt);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_close(db);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    return count > 0;
}


void store_results(void)
{
    // Sqlite3 
    sqlite3 *db;
    int rc;
    char *zErrMsg = 0;
    sqlite3_stmt *stmt;

    rc = sqlite3_open(DBPATH, &db);
    if(rc) {
        fprintf(stderr, "\nError: Can't open database in %s:%i: %s\n", __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    sqlite3_busy_timeout(db, 100000);

    rc = sqlite3_exec(db, 
        "CREATE TABLE IF NOT EXISTS " TABLENAME " "
        "(N INTEGER, gamma REAL, beta REAL, lf REAL, "
        "magnetization_mean REAL, magnetization_variance REAL, "
        "magnetization_skewness REAL, magnetization_kurtosis REAL, "
        "magnetization_A_mean REAL, magnetization_A_variance REAL, "
        "magnetization_A_skewness REAL, magnetization_A_kurtosis REAL, "
        "S_AB REAL, S2_AB REAL, Smin_AB REAL, "
        "S_A REAL, S2_A REAL, Smin_A REAL, "
        "energy REAL, version TEXT,"
        "CONSTRAINT " TABLENAME "_uniq UNIQUE "
        "(N, gamma, beta, lf))", NULL, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, zErrMsg);
        sqlite3_free(zErrMsg);
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_prepare_v2(db, 
        "INSERT INTO " TABLENAME " (N, gamma, beta, lf, "
        "magnetization_mean, magnetization_variance, "
        "magnetization_skewness, magnetization_kurtosis, "
        "magnetization_A_mean, magnetization_A_variance, "
        "magnetization_A_skewness, magnetization_A_kurtosis, "
        "S_AB, S2_AB, Smin_AB, S_A, S2_A, Smin_A, energy, version) "
        "VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18, ?19, ?20);", 
        -1, &stmt, NULL);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_int(stmt, 1, N);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 2, Gamma);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 3, beta);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 4, lf);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 5, magnetization_mean);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 6, magnetization_variance);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 7, magnetization_skewness);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 8, magnetization_kurtosis);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 9, magnetization_A_mean);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 10, magnetization_A_variance);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 11, magnetization_A_skewness);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 12, magnetization_A_kurtosis);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 13, S_AB);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 14, S2_AB);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 15, Smin_AB);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 16, S_A);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 17, S2_A);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 18, Smin_A);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 19, energy);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_text(stmt, 20, GIT_DESCRIBE, sizeof(GIT_DESCRIBE), SQLITE_STATIC);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_step(stmt);
    if(rc != SQLITE_OK && rc != SQLITE_DONE) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        if(rc == SQLITE_CONSTRAINT)
        {
            sqlite3_finalize(stmt);
            sqlite3_close(db);
            return;
        }
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_reset(stmt);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_finalize(stmt);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_close(db);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }
}
