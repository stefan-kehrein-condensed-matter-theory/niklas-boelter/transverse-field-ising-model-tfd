#!/usr/bin/env python3
import sys
quenches = [(0.25, 0.75, 0.55), (0.25, 0.40, 0.55), (0.75, 0.25, 0.0), (0.75, 0.60, 0.0)]

start = 0
step = 1
stop = 100

for N in [32, 16, 14, 12, 10, 8, 6, 4]:
    for quench in quenches:
        Gamma_i = quench[0]
        Gamma_f = quench[1]
        lf = quench[2]
        for beta in [0, 4, 16, 1024]:
            print("{} {} {} {} {} {} {} {} {}".format(sys.argv[1], N, Gamma_i, Gamma_f, beta, start, step, stop, lf))
