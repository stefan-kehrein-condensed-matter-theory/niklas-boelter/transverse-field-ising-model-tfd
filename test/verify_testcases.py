#!/usr/bin/env python3
import sqlite3
import sys
import numpy

v1 = sys.argv[1]
v2 = sys.argv[2]

con = sqlite3.connect('FC-TFIM-QUENCH.db')
c = con.cursor()

c.execute('''
    SELECT r1.N, r1.gamma_i, r1.gamma_f, r1.beta, r1.t, r1.lfi,
        r1.magnetization - r2.magnetization,
        r1.magnetization_err - r2.magnetization_err,
        r1.S_A - r2.S_A,
        r1.S_AB - r2.S_AB,
        r1.S_AC - r2.S_AC,
        r1.S_C - r2.S_C,
        r1.S2_A - r2.S2_A,
        r1.S2_AB - r2.S2_AB,
        r1.S2_AC - r2.S2_AC,
        r1.S2_C - r2.S2_C,
        r1.Smin_A - r2.Smin_A,
        r1.Smin_AB - r2.Smin_AB,
        r1.Smin_AC - r2.Smin_AC,
        r1.Smin_C - r2.Smin_C
    FROM results_{} r1, results_{} r2
    WHERE
        r1.N = r2.N
        AND r1.gamma_i = r2.gamma_i
        AND r1.gamma_f = r2.gamma_f
        AND r1.beta = r2.beta
        AND r1.t = r2.t
        AND r1.lfi  = r2.lfi'''.format(v1, v2))

max_max_deviation = 0
number_tests = 0

for row in c:
    number_tests = number_tests + 1
    parameters = row[0:6]
    max_deviation = numpy.max(numpy.abs(row[6:]))
    if max_deviation > max_max_deviation:
        max_max_deviation = max_deviation

print('Tests run:', number_tests, '/', 12928)
print('Max deviation:', max_max_deviation)
if max_deviation > 1E-12 or number_tests < 12928:
    print('FAIL FAIL FAIL')
    exit(1)
else:
    print('PASS')
