#!/usr/bin/env python3

import asyncio
import datetime
import fileinput
import logging
import math
import sys

nfail = 0
nproc = 0
status = ""
lock = asyncio.Lock()


def report_status():
    global status
    print(status, end='')


def lines(argv):
    global nproc, status
    times = []
    lines = []
    for line in fileinput.input(argv):
        line = line.strip()
        if len(line) > 0:
            lines.append(line)

    start = datetime.datetime.now()

    i = 0
    total = len(lines)

    format_str = ("\033[2K\r\033[97m{0:0"
                  + str(math.ceil(math.log(total + 1) / math.log(10)))
                  + "}\033[00m/{1} done, "
                  + "\033[34m{2}\033[00m elapsed, "
                  + "\033[31m{3}\033[00m remaining  ")

    for line in lines:
        i = i + 1
        finished = max(i - nproc, 0)
        elapsed = datetime.datetime.now() - start
        remaining = "??"

        if finished != 0:
            remaining = elapsed * (total - finished) / finished
            remaining = datetime.timedelta(
                days=remaining.days, seconds=remaining.seconds, microseconds=0)
            times.append(datetime.datetime.now())

        # Be a bit "smarter" about the remaining time!
        if len(times) > 4 * nproc + 1:
            remaining = (times[-1] - times[-4 * nproc - 1]) * \
                (total - finished) / (4 * nproc)
            remaining = datetime.timedelta(
                days=remaining.days, seconds=remaining.seconds, microseconds=0)

        elapsed = datetime.timedelta(
            days=elapsed.days, seconds=elapsed.seconds, microseconds=0)

        status = format_str.format(finished, total, elapsed, remaining)
        report_status()
        yield line.strip()


async def run_all(lines):
    global nproc, status
    active = set()
    i = 1
    while True:
        while len(active) < nproc:
            try:
                command = lines.__next__()
            except StopIteration:
                break

            proc = await asyncio.create_subprocess_shell(command, stdout=asyncio.subprocess.DEVNULL,
                                                         stderr=asyncio.subprocess.PIPE)
            logging.info("Started command '{}'.".format(command))
            active.add(wait_for_exit(i, command, proc))
            i = i + 1

        if len(active) == 0:
            break

        (_, active) = await asyncio.wait(active, return_when=asyncio.FIRST_COMPLETED)
        status = "\033[2K\rWaiting for processes to finish: {0} left\033[K".format(len(active))
        report_status()


async def wait_for_exit(i, command, proc):
    global nfail, lock

    while not proc.stderr.at_eof():
        line = await proc.stderr.readline()
        line = line.decode('utf-8').rstrip()
        if line:
            error_message = "Error output from command #{} '\033[34m{}\033[00m': \033[31m{}\033[00m".format(
                            i, command, line)
            logging.info(error_message)
            print("\033[2K\r", end='')
            print(error_message)
            report_status()

    await proc.wait()

    if proc.returncode == 0:
        logging.info("Command #{} '{}' completed successfully".format(i, command))
    else:
        error_message = "Command #{} '{}' failed with exit code {}".format(i, command, proc.returncode)
        logging.info(error_message)
        print("\033[2K\r", end='')
        print("\033[31m" + error_message + "\033[00m")
        report_status()
        async with lock:
            nfail = nfail + 1


def main():
    global nproc, nfail
    if(len(sys.argv) < 2):
        print("{0} <N>\nAccepts a list of commands and executes N of them in parallel.".format(
            sys.argv[0]))
        return

    nproc = int(sys.argv[1])
    start = datetime.datetime.now()

    # logging.getLogger().setLevel(logging.DEBUG)

    asyncio.run(run_all(lines(sys.argv[2:])))

    print("\033[2K\rTotal time taken: {}{}".format(
        datetime.datetime.now() - start, ' ' * 20))
    if nfail > 0:
        print("\033[31m{} processes failed!\033[00m".format(nfail))
    exit(nfail)


if __name__ == '__main__':
    main()
