#!/usr/bin/env fish

cd (dirname (status filename))
set git_describe (git describe --always --dirty|tr -d "\n"|tr -C [:alnum:]_ _)
set executable ../transverse-field-ising-model-tfd

if test ! -f $executable; 
	echo "FAIL $executable does not exist" >&2
	exit
end

if ! $executable | grep -c  $git_describe > /dev/null
	echo "FAIL $executable version mismatch" >&2
	exit
end

rm -v FC-TFIM-QUENCH.db

./generate-testcases.py $executable > run_tests.txt

if ! ./run.py 1 run_tests.txt
	echo "FAIL"
	exit
end

rm run_tests.txt

sqlite3 FC-TFIM-QUENCH.db < test_data_0401e66.sql

exec ./verify_testcases.py 0401e66 $git_describe
