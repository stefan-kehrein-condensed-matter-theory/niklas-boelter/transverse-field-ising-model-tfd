/*  Copyright 2019 Niklas Bölter <niklas.boelter@theorie.physik.uni-goettingen.de> */

#include <gmp.h>
#include "util.h"
#include "sha-256.h"

clock_t start;
int argv_len; // Used for status reporting
char *argv_ptr;
char *status_prefix; // Prefix string for status reporting

void * my_calloc(long long count, size_t size)
{
    void *ptr;

    ptr = calloc((size_t) count, size);
    
    #ifdef DEBUG
    printf("\nCalled my_calloc(%lli, %zu) = %p\n", count, size, ptr);
    #endif

    return ptr;
}

void * my_memset(void *b, int c, long long count, size_t size)
{
    #ifdef DEBUG
    printf("\nCalled my_memset(%p, %i, %lli, %zu)\n", b, c, count, size);
    #endif
    
    memset(b, c, (size_t) count * size);

    return b;
}


void show_rho_eigenvalues(const double *rho_eigenvalues, long long dim)
{
    int i;
    double trace;

    trace = 0.0;
    for(i = 0; i < dim; i++)
    {
        trace += rho_eigenvalues[i];
    }
    printf("Trace: %g\n  ", trace);

    printf("Eigenvalues: ");
    for(i = 0; i < dim; i++)
    {
        if(fabs(rho_eigenvalues[i]) > 1E-12)
        {
            printf("%g ", rho_eigenvalues[i]);
        }
    }
    printf("\n");
}


double calculate_Renyi_entropy(const double *eigenvalues, long long dim, double alpha)
{
    int i;
    double sum = 0.0;
    double sum_positive = 0.0;
    double sum_negative = 0.0;
    for(i = 0; i < dim; i++)
    {
        if (eigenvalues[i] > 0.0)
        {
            sum += pow(eigenvalues[i], alpha);
            sum_positive += eigenvalues[i];
        }
        else
        {
            sum_negative -= eigenvalues[i];
        }
    }
    if(fabs(sum_positive - 1.0) > 1E-6)
    {
        fprintf(stderr, "calculate_Renyi_entropy: Sum of positive Eigenvalues != 1\n");
        show_rho_eigenvalues(eigenvalues, dim);
        exit(EX_SOFTWARE);
    }
    if(fabs(sum_negative) > 1E-6)
    {
        fprintf(stderr, "calculate_Renyi_entropy: Negative Eigenvalues\n");
        show_rho_eigenvalues(eigenvalues, dim);
        exit(EX_SOFTWARE);
    }
    return log2(sum)/(1-alpha);
}


double calculate_vonNeumann_entropy(const double *eigenvalues, long long dim)
{
    int i;
    double SvN = 0.0;
    double sum_positive = 0.0;
    double sum_negative = 0.0;
    for(i = 0; i < dim; i++)
    {
        if (eigenvalues[i] > 0.0)
        {
            SvN = SvN - eigenvalues[i]*log2(eigenvalues[i]);
            sum_positive += eigenvalues[i];
        }
        else
        {
            sum_negative -= eigenvalues[i];
        }
    }
    if(fabs(sum_positive - 1.0) > 1E-6)
    {
        fprintf(stderr, "\ncalculate_vonNeumann_entropy: Sum of positive Eigenvalues != 1\n");
        show_rho_eigenvalues(eigenvalues, dim);
       exit(EX_SOFTWARE);
    }
    if(fabs(sum_negative) > 1E-6)
    {
        fprintf(stderr, "\ncalculate_vonNeumann_entropy: Negative Eigenvalues\n");
        show_rho_eigenvalues(eigenvalues, dim);
       exit(EX_SOFTWARE);
    }
    return SvN;
}


double calculate_min_entropy(const double *eigenvalues, long long dim)
{
    int i;
    double max_eigenvalue = 0.0;
    double sum_positive = 0.0;
    double sum_negative = 0.0;
    for(i = 0; i < dim; i++)
    {
        if (eigenvalues[i] > 0.0)
        {
            sum_positive += eigenvalues[i];
        }
        else
        {
            sum_negative -= eigenvalues[i];
        }

        if(eigenvalues[i] > max_eigenvalue)
        {
            max_eigenvalue = eigenvalues[i];
        }
    }
    if(fabs(sum_positive - 1.0) > 1E-6)
    {
        fprintf(stderr, "\ncalculate_min_entropy: Sum of positive Eigenvalues != 1\n");
        show_rho_eigenvalues(eigenvalues, dim);
        exit(EX_SOFTWARE);
    }
    if(fabs(sum_negative) > 1E-6)
    {
        fprintf(stderr, "\ncalculate_min_entropy: Negative Eigenvalues\n");
        show_rho_eigenvalues(eigenvalues, dim);
        exit(EX_SOFTWARE);
    }
    return -1.0 * log2(max_eigenvalue);
}


/* 
 * After finding a serious bug in the lapacke interface I decided to call LAPACK directly
 */

/*
void diagonalize(double _Complex *M, lapack_int dim, double _Complex *eigenvalues)
{
    lapack_int ret;
    start = clock();

    ret = LAPACKE_zgeev(LAPACK_ROW_MAJOR, 'N', 'N', dim, M, dim, eigenvalues, NULL, 1, NULL, 1);

    if(ret)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    // report_elapsed_time();
}


void diagonalize_hermitian_eigenvectors(double _Complex *M, lapack_int dim, double *eigenvalues)
{
    lapack_int ret;

    start = clock();

    ret = LAPACKE_zheev(LAPACK_ROW_MAJOR, 'V', 'U', dim, M, dim, eigenvalues);

    if(ret)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }

    report_elapsed_time();
}


void diagonalize_hermitian(double _Complex *M, lapack_int dim, double *eigenvalues)
{
    lapack_int ret;

    start = clock();

    ret = LAPACKE_zheev(LAPACK_ROW_MAJOR, 'N', 'U', dim, M, dim, eigenvalues);

    if(ret)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    
    report_elapsed_time();
}


void diagonalize_symmetric_eigenvectors(double *M, lapack_int dim, double *eigenvalues)
{
    lapack_int ret;

    start = clock();

    ret = LAPACKE_dsyev(LAPACK_ROW_MAJOR, 'V', 'U', dim, M, dim, eigenvalues);

    if(ret)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    
    report_elapsed_time();
}


void diagonalize_symmetric(double *M, lapack_int dim, double *eigenvalues)
{
    lapack_int ret;

    start = clock();

    ret = LAPACKE_dsyev(LAPACK_ROW_MAJOR, 'N', 'U', dim, M, dim, eigenvalues);

    if(ret)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    
    report_elapsed_time();
}
*/

void diagonalize(double _Complex *M, lapack_int dim, double _Complex *eigenvalues)
{
    /* WARNING: LAPACK USES COLUMN MAJOR ORDER */

    /* Lapacke variables */
    lapack_int ret, lwork;
    double _Complex workopt;
    double _Complex *work;
    double *rwork;
    lapack_int dvr = 1;

    start = clock();

    my_memset(eigenvalues, 0, dim, sizeof(double _Complex));

    rwork = my_calloc(2 * dim, sizeof(double));

    if(!rwork)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    lwork = -1;
    zgeev_("None", "None", &dim, M, &dim, eigenvalues, 
        NULL, &dvr, NULL, &dvr, &workopt, &lwork, rwork, &ret);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Workspace query failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }

    lwork = (int) workopt;

    work = my_calloc(lwork, sizeof(double _Complex));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    zgeev_("None", "None", &dim, M, &dim, eigenvalues, 
        NULL, &dvr, NULL, &dvr, work, &lwork, rwork, &ret);

    free(rwork);
    free(work);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    report_elapsed_time();
}


void diagonalize_hermitian_eigenvectors(double _Complex *M, lapack_int dim, double *eigenvalues)
{
    /* WARNING: LAPACK USES COLUMN MAJOR ORDER */

    /* Lapacke variables */
    lapack_int ret, lwork;
    double _Complex workopt;
    double _Complex *work;
    double *rwork;

    start = clock();

    my_memset(eigenvalues, 0, dim, sizeof(double));

    rwork = my_calloc(3 * dim, sizeof(double));

    if(!rwork)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    lwork = -1;
    zheev_("Vectors", "Upper", &dim, M, &dim, eigenvalues, 
        &workopt, &lwork, rwork, &ret);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Workspace query failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }

    lwork = (int) workopt;

    work = my_calloc(lwork, sizeof(double _Complex));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    zheev_("Vectors", "Upper", &dim, M, &dim, eigenvalues, 
        work, &lwork, rwork, &ret);

    free(rwork);
    free(work);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    report_elapsed_time();
}


void diagonalize_hermitian(double _Complex *M, lapack_int dim, double *eigenvalues)
{
    /* WARNING: LAPACK USES COLUMN MAJOR ORDER */

    /* Lapacke variables */
    lapack_int ret, lwork;
    double _Complex workopt;
    double _Complex *work;
    double *rwork;

    start = clock();

    my_memset(eigenvalues, 0, dim, sizeof(double));

    rwork = my_calloc(3 * dim, sizeof(double));

    if(!rwork)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    lwork = -1;
    zheev_("None", "Upper", &dim, M, &dim, eigenvalues, 
        &workopt, &lwork, rwork, &ret);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Workspace query failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }

    lwork = (int) workopt;

    work = my_calloc(lwork, sizeof(double _Complex));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    zheev_("None", "Upper", &dim, M, &dim, eigenvalues, 
        work, &lwork, rwork, &ret);

    free(rwork);
    free(work);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    report_elapsed_time();
}


void diagonalize_symmetric_eigenvectors(double *M, lapack_int dim, double *eigenvalues)
{
    /* WARNING: LAPACK USES COLUMN MAJOR ORDER */

    /* Lapacke variables */
    lapack_int ret, lwork;
    double workopt;
    double *work;

    start = clock();

    my_memset(eigenvalues, 0, dim, sizeof(double));

    lwork = -1;
    dsyev_("Vectors", "Upper", &dim, M, &dim, eigenvalues, &workopt, &lwork, &ret);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Workspace query failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }

    lwork = (int) workopt;

    work = my_calloc(lwork, sizeof(double));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    dsyev_("Vectors", "Upper", &dim, M, &dim, eigenvalues, work, &lwork, &ret);

    free(work);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    report_elapsed_time();
}


void diagonalize_symmetric(double *M, lapack_int dim, double *eigenvalues)
{
    /* WARNING: LAPACK USES COLUMN MAJOR ORDER */

    /* Lapacke variables */
    lapack_int ret, lwork;
    double workopt;
    double *work;

    start = clock();

    my_memset(eigenvalues, 0, dim, sizeof(double));

    lwork = -1;
    dsyev_("None", "Upper", &dim, M, &dim, eigenvalues, &workopt, &lwork, &ret);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Workspace query failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }

    lwork = (int) workopt;

    work = my_calloc(lwork, sizeof(double));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    dsyev_("None", "Upper", &dim, M, &dim, eigenvalues, work, &lwork, &ret);

    free(work);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    
    report_elapsed_time();
}


void report_status(const char *status)
{
    char buf[2048];

    memset(buf, '\0', 2048);

    if(status_prefix)
    {
        snprintf(buf, sizeof(buf), "%s%s", status_prefix, status);
    }
    else
    {
        snprintf(buf, sizeof(buf), "%s", status);
    }

    if(argv_ptr)
    {
        my_memset(argv_ptr, '\0', argv_len, sizeof(char));
        snprintf(argv_ptr, (size_t) argv_len, "%s", buf);
    }
    printf("\r%s", buf);
    fflush(stdout);
}


void report_elapsed_time()
{
    clock_t stop = clock();
    long ms = (stop - start) * 1000 / CLOCKS_PER_SEC;

    if(ms > 1000*60*60)
    {
        printf("# Time taken: %4ld hours %2ld minutes %2ld seconds %4ld milliseconds\n",
        ((ms/1000)/60)/60, ((ms/1000)/60)%60, (ms/1000)%60, ms%1000);
    }
    else if(ms > 1000*60)
    {
        printf("# Time taken: %2ld minutes %2ld seconds %4ld milliseconds\n",
        ((ms/1000)/60)%60, (ms/1000)%60, ms%1000);
    }
    else
    {
        printf("# Time taken: %2ld seconds %4ld milliseconds\n",
        (ms/1000)%60, ms%1000);
    }
    
}


void check_normalized(const double *V, long long N)
{
    int i;
    double squaredNorm = 0.0;
    for(i = 0; i < N; i++)
    {
        squaredNorm += V[i] * V[i];
    }

    if(fabs(squaredNorm - 1) > 1E-12)
    {
        fprintf(stderr, "\nError: V is not normalized!\n");
        exit(EX_SOFTWARE);
    }
    // printf("\nNormalized ✔︎\n");
}


void check_normalized_complex(const double _Complex *V, long long N)
{
    int i;
    double squaredNorm = 0.0;
    for(i = 0; i < N; i++)
    {
        squaredNorm += creal(V[i] * conj(V[i]));
    }

    if(fabs(squaredNorm - 1) > 1E-12)
    {
        fprintf(stderr, "\nError: V is not normalized! %g \n", squaredNorm);
        exit(EX_SOFTWARE);
    }
    // printf("\nNormalized ✔︎\n");
}


void check_unitary(const double _Complex *U, long long N)
{
    int i,j,k;
    double _Complex matrixElement;

    for(i = 0; i < N; i++)
    {
        for(j = 0; j < N; j++)
        {
            matrixElement = 0.0 + 0.0 * (double _Complex) _Complex_I;
            for(k = 0; k < N; k++)
            {
                matrixElement += U[i + N * k] * conj(U[j + N * k]);
            }

            if((i == j && cabs(matrixElement - 1.0) > 1E-12) || 
               (i != j && cabs(matrixElement) > 1E-12))
            {
                fprintf(stderr, "\nError: O is not unitary!\n");
                exit(EX_SOFTWARE);
            }
        }
    }
    printf("\nUnitary ✔︎\n");
}


void check_hermitian(const double _Complex *rho, long long N)
{
    int i,j;

    for(i = 0; i < N; i++)
    {
        for(j = 0; j < N; j++)
        {
            if(cabs(rho[i + N * j] - conj(rho[j + N * i])) > 1E-12)
            {
                fprintf(stderr, "\nError: rho is not hermitian!\n");
                exit(EX_SOFTWARE);
            }
        }
    }
    printf("\nHermitian ✔︎\n");
}


void check_symmetric(const double *rho, long long N)
{
    int i,j;

    for(i = 0; i < N; i++)
    {
        for(j = 0; j < N; j++)
        {
            if(fabs(rho[i + N * j] - rho[j + N * i]) > 1E-12)
            {
                fprintf(stderr, "\nError: rho is not symmetric!\n");
                exit(EX_SOFTWARE);
            }
        }
    }
    printf("\nSymmetric ✔︎\n");
}


void show_checksum(const void *data, size_t size)
{
    int i;
    uint8_t hash[32];
    char hash_string[65];

    char *hash_string_ptr = &(hash_string[0]);

    start = clock();

    calc_sha_256(hash, data, size);

    for (i = 0; i < 32; i++) {
        hash_string_ptr += sprintf(hash_string_ptr, "%02x", hash[i]);
    }
    
    printf("Checksum  %s  ", hash_string);
    report_elapsed_time();
}


double binomial_prefactor(int spins_A, int spins_B, int spinsUp_A, int spinsUp_B)
{
    double ret;
    mpz_t a, b, ab, total;
    mpz_init(a);
    mpz_bin_uiui(a, (unsigned int) spins_A, (unsigned int) spinsUp_A);

    mpz_init(b);
    mpz_bin_uiui(b, (unsigned int) spins_B, (unsigned int) spinsUp_B);

    mpz_init(total);
    mpz_bin_uiui(total, (unsigned int) spins_A + (unsigned int) spins_B, 
                        (unsigned int) spinsUp_A + (unsigned int) spinsUp_B);

    mpz_init(ab);
    mpz_mul(ab, a, b);
    mpz_clear(a);
    mpz_clear(b);

    mpf_t numerator, denominator, fraction, result;
    mpf_init(numerator);
    mpf_set_z(numerator, ab);
    mpz_clear(ab);

    mpf_init(denominator);
    mpf_set_z(denominator, total);
    mpz_clear(total);

    mpf_init(fraction);
    mpf_div(fraction, numerator, denominator);
    mpf_clear(numerator);
    mpf_clear(denominator);

    mpf_init(result);
    mpf_sqrt(result, fraction);
    mpf_clear(fraction);

    ret = mpf_get_d(result);
    mpf_clear(result);

    return ret;
}
