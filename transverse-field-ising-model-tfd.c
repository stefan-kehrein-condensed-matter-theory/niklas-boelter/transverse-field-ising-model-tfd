/*  Copyright 2019 Niklas Bölter <niklas.boelter@theorie.physik.uni-goettingen.de> */
#include "util.h"

#ifdef HAAR_RANDOM_UNITARY
#include "random-unitary.h"
#define TABLENAME "results_Haar_" GIT_DESCRIBE
#else
#define TABLENAME "results_" GIT_DESCRIBE
#endif

#include <assert.h>
#include <sqlite3.h>

#define DBPATH "FC-TFIM-QUENCH.db"

// System Parameters
static int N = 60;
static int nStates = 0;
static double Gamma_i = 0.25;
static double Gamma_f = 0.75;
static double dt = 1.0;
static double lfi = 0.0;
static double starttime = 0.0;
static double t;
static double stoptime  = 100.0;

// Observables
static double magnetization;
static double magnetization_err;
static double magnetization_C_mean;
static double magnetization_C_variance;
static double magnetization_C_skewness;
static double magnetization_C_kurtosis;
static double S_A, S_AB, S_AC, S_C;
static double S2_A, S2_AB, S2_AC, S2_C;
static double Smin_A, Smin_AB, Smin_AC, Smin_C;

static double *binomial_prefactors_AB;
static double *binomial_prefactors_CD;

static int spins_A, spins_B, spins_C, spins_D;
static double *hamiltonian_i;
static double *eigenvalues_i;
static double *hamiltonian_f;
static double *eigenvalues_f;
static double _Complex *tfd_state;

#ifdef HAAR_RANDOM_UNITARY
static double _Complex *random_unitary;
static double _Complex *rotated_eigenvectors;
#endif // ifdef HAAR_RANDOM_UNITARY

static double *rho_AB;
static double *rho_AB_eigenvalues;

static double *rho_A;
static double _Complex *rho_C;
static double *rho_A_eigenvalues;
static double *rho_C_eigenvalues;


static double _Complex *rho_AC;
static double _Complex *rho_CD;
static double *rho_AC_eigenvalues;
static double *rho_CD_eigenvalues;



static int find_results(void);
static void store_results(void);
static void usage(char* argv0);

void usage(char *argv0)
{
    printf("Usage: %s N gamma_i gamma_f nStates start dt stop lfi\n\n", argv0);
    printf("Calculates tripartite information of |U(t)> state\n");
    printf("System: fully connected TFIM N/2:N/2 partition\n\n");
    printf("N\t Number of spins, should be even\n");
    printf("Gamma_i\t Initial coupling strength\n");
    printf("Gamma_f\t Coupling strength for time evolution\n");
    printf("nStates\t Number of lowest states to include in initial state\n");
    printf("start\t Start time\n");
    printf("dt\t Time difference between sampling points\n");
    printf("stop\t Stop time\n");
    printf("lfi\t Strength of initial longitudinal field\n");
#ifdef HAAR_RANDOM_UNITARY
    printf("\n\033[31mThis is a modified version that\n");
    printf("uses a random unitary for time evolution!\033[0m\n");
    printf("The used random number generator is arc4random.");
#endif // ifdef HAAR_RANDOM_UNITARY

#ifdef USE_INTEL_MKL
    MKLVersion Version;
 
    mkl_get_version(&Version);
    printf("\n\n"); 
    printf("MKL version %d.%d.%d build %s\n", Version.MajorVersion, Version.MinorVersion, Version.UpdateVersion, Version.Build);
    printf("%s\n", Version.Processor); 
#endif
    
    printf("\n\nVersion %s (compiler version %s)\n", GIT_DESCRIBE, __VERSION__);
}

int main(int argc, char *argv[])
{
    clock_t init;
    char buf[1024];
    char buf2[1024];
    long long i,j,k,l;
    int step,number_of_steps;
    double magnetization_squared;
    int spinsUp, spinsUp_A, spinsUp_B, spinsUp_C, spinsUp_D;

    long long A_row,C_row,A_column,C_column,B,D;
    long long dim, dim_A, dim_B, dim_C, dim_D;
    double energy_i, energy_i_squared;
    double energy_f, energy_f_squared;


#if defined(USE_INTEL_MKL) && defined(DEBUG)
    mkl_verbose(1);
#endif


#ifdef USE_INTEL_MKL
    if(mkl_enable_instructions(MKL_ENABLE_AVX2) != 1)
    {
        fprintf(stderr, "WARNING: mkl_enable_instructions(MKL_ENABLE_AVX2) was rejected by MKL\n");
    }
    mkl_verbose(1);
#endif // ifdef USE_INTEL_MKL

    
    init = clock();

    if(argc != 9)
    {
        usage(argv[0]);
        exit(EX_USAGE);
    }

    N = atoi(argv[1]);
    Gamma_i = atof(argv[2]);
    Gamma_f = atof(argv[3]);
    nStates = atoi(argv[4]);
    starttime = atof(argv[5]);
    dt = atof(argv[6]);
    stoptime = atof(argv[7]);

    lfi = atof(argv[8]);

    spins_A = spins_B = spins_C = spins_D = N/2;

    dim = N+1;
    dim_A = spins_A+1;
    dim_B = spins_B+1;
    dim_C = spins_C+1;
    dim_D = spins_D+1;

    if(N < 1 || N % 2)
    {
        fprintf(stderr, "Parameter N should be positive and even.");
        exit(EX_USAGE);
    }
    if(nStates < 2)
    {
        fprintf(stderr, "The number of lowest states to include must be larger than 1.");
        exit(EX_USAGE);
    }

    assert(spins_A <= spins_B);
    assert(spins_A + spins_B == N);
    assert(spins_C + spins_D == N);

    argv_len = 0;
    for(i = 0; i < argc; i++)
    {
        argv_len += strlen(argv[i]) + 1;
    }
    argv_ptr = &argv[0][0];
    buf[0] = '\0';
    status_prefix = buf;

    double required_memory = 0.0;
    required_memory += dim_A * dim_B;
    required_memory += dim_C * dim_D;
    required_memory += 2 * dim * dim;
    required_memory += 2 * dim;
    required_memory += dim * dim * 2;

    required_memory += dim * dim;
    required_memory += (dim_A + 1) * dim_A;
    if(dt >= 0.0)
    {
        required_memory += 1LL * dim_A * dim_C * dim_A * dim_C * 2;
        required_memory += dim_A * dim_C;
    }
#ifdef HAAR_RANDOM_UNITARY
    required_memory +=  4.0 * dim * dim * 2;
#endif
    required_memory = required_memory * (double) sizeof(double);

    printf("Version %s (compiler version %s)\n", GIT_DESCRIBE, __VERSION__);
    printf("I will need %i MiB of heap memory.\n", (int) (required_memory/(1024*1024)+1));
    report_status("Initializing ");
    
    binomial_prefactors_AB = my_calloc(dim_A * dim_B, sizeof(double));
    binomial_prefactors_CD = my_calloc(dim_C * dim_D, sizeof(double));
    hamiltonian_i = my_calloc(dim * dim, sizeof(double));
    eigenvalues_i = my_calloc( dim, sizeof(double));
    hamiltonian_f = my_calloc(dim * dim, sizeof(double));
    eigenvalues_f = my_calloc( dim, sizeof(double));
    tfd_state = my_calloc(dim * dim, sizeof(double _Complex));

    rho_AB = my_calloc(dim * dim, sizeof(double));
    rho_AB_eigenvalues = my_calloc(dim, sizeof(double));

    rho_A = my_calloc(dim_A * dim_A, sizeof(double));
    rho_A_eigenvalues = my_calloc(dim_A, sizeof(double));

    rho_C = my_calloc(dim_C * dim_C, sizeof(double _Complex));
    rho_C_eigenvalues = my_calloc(dim_C, sizeof(double));

    if(!binomial_prefactors_AB || !binomial_prefactors_CD || !hamiltonian_i
        || !eigenvalues_i || !hamiltonian_f || !eigenvalues_f 
        || !tfd_state || !rho_AB || !rho_AB_eigenvalues|| !rho_A 
        || !rho_A_eigenvalues)
    {
        fprintf(stderr, "ERROR: Could not allocate memory.\n");
        exit(EX_OSERR);
    }

    if(dt >= 0.0)
    {
        rho_AC = my_calloc(dim_A * dim_C * dim_A * dim_C, sizeof(double _Complex));
        rho_AC_eigenvalues = my_calloc(dim_A * dim_C, sizeof(double));

        rho_CD = rho_AC;
        rho_CD_eigenvalues = rho_AC_eigenvalues;
        if(!rho_AC || !rho_AC_eigenvalues)
        {
            fprintf(stderr, "ERROR: Could not allocate memory.\n");
            exit(EX_OSERR);
        }
    }

#ifdef HAAR_RANDOM_UNITARY
    random_unitary = my_calloc(dim * dim, sizeof(double _Complex));
    rotated_eigenvectors = my_calloc(dim * dim, sizeof(double _Complex));
    if(!random_unitary || !rotated_eigenvectors)
    {
        fprintf(stderr, "ERROR: Could not allocate memory.\n");
        exit(EX_OSERR);
    }
#endif // ifdef HAAR_RANDOM_UNITARY

    // See https://arxiv.org/abs/1908.02596 for details on the model and bipartition
    
    // Precalculate the prefactors
    for(spinsUp_A = 0; spinsUp_A <= spins_A; spinsUp_A++)
    {
        for(spinsUp_B = 0; spinsUp_B <= spins_B; spinsUp_B++)
        {
            binomial_prefactors_AB[spinsUp_A + dim_A * spinsUp_B] = 
                binomial_prefactor(spins_A, spins_B, spinsUp_A, spinsUp_B);
        }
    }

    for(spinsUp_C = 0; spinsUp_C <= spins_C; spinsUp_C++)
    {
        for(spinsUp_D = 0; spinsUp_D <= spins_D; spinsUp_D++)
        {
            binomial_prefactors_CD[spinsUp_C + dim_C * spinsUp_D] = 
                binomial_prefactor(spins_C, spins_D, spinsUp_C, spinsUp_D);
        }
    }

    // Construct Hamiltonian
    for(spinsUp = 0; spinsUp < dim; spinsUp++)
    {
        double s = (double) spinsUp / (double) N - 0.5;
        hamiltonian_i[spinsUp + dim * spinsUp] = -0.5 * s * s - lfi * s/N;
        hamiltonian_f[spinsUp + dim * spinsUp] = -0.5 * s * s; //- lfi * s/N;
        
        if(spinsUp < N)
        {
            hamiltonian_i[(spinsUp+1) + dim * spinsUp] = -0.5 * Gamma_i 
                * sqrt(0.25 - s*s + (0.5-s)/N);
            hamiltonian_f[(spinsUp+1) + dim * spinsUp] = -0.5 * Gamma_f 
                * sqrt(0.25 - s*s + (0.5-s)/N);

        }

        if(spinsUp > 0)
        {
            hamiltonian_i[(spinsUp-1) + dim * spinsUp] = -0.5 * Gamma_i 
                * sqrt(0.25 - s*s + (0.5+s)/N);
            hamiltonian_f[(spinsUp-1) + dim * spinsUp] = -0.5 * Gamma_f 
                * sqrt(0.25 - s*s + (0.5+s)/N);
        }
    }

#ifdef DEBUG
    check_symmetric(hamiltonian_i, dim);
    check_symmetric(hamiltonian_f, dim);
#endif // ifdef DEBUG
    report_elapsed_time();

    start = clock();
    
    snprintf(buf2, sizeof(buf2), "Diagonalizing initial Hamiltonian (%lli x %lli) ", dim, dim);
    report_status(buf2);
    diagonalize_symmetric_eigenvectors(hamiltonian_i, (lapack_int) dim, eigenvalues_i);

    snprintf(buf2, sizeof(buf2), "Diagonalizing final Hamiltonian (%lli x %lli) ", dim, dim);
    report_status(buf2);
    diagonalize_symmetric_eigenvectors(hamiltonian_f, (lapack_int) dim, eigenvalues_f);

    start = clock();
    report_status("Calculating rho_AB, rho_A ");

    double min_eigenvalue_i = (double) +INFINITY;
    double max_eigenvalue_i = (double) -INFINITY;

    double min_eigenvalue_f = (double) +INFINITY;
    double max_eigenvalue_f = (double) -INFINITY;

    // For the Boltzmann factors we shift the energy to be non-negative,
    // this prevents numerical issues at very low temperatures.
    for(k = 0; k < dim; k++)
    {
        printf("Eigenvalue: %lf\n", eigenvalues_i[k]);

        if(eigenvalues_i[k] < min_eigenvalue_i)
        {
            min_eigenvalue_i = eigenvalues_i[k];
        }

        if(eigenvalues_i[k] > max_eigenvalue_i)
        {
            max_eigenvalue_i = eigenvalues_i[k];
        }

        if(eigenvalues_f[k] < min_eigenvalue_f)
        {
            min_eigenvalue_f = eigenvalues_f[k];
        }

        if(eigenvalues_f[k] > max_eigenvalue_f)
        {
            max_eigenvalue_f = eigenvalues_f[k];
        }
    }

    double partitionSum = 0.0;
    for(k = 0; k < nStates; k++)
    {
        partitionSum += 1;
        // exp(-1.0 * beta * N * (eigenvalues_i[k]-min_eigenvalue_i));
    }

    energy_i = 0.0;
    energy_i_squared = 0.0;

    for(k = 0; k < nStates; k++)
    {
        for(i = 0; i < dim; i++)
        {
            for(j = 0; j < dim; j++)
            {
                rho_AB[i + dim * j] +=
                      hamiltonian_i[i + dim * k]
                    * hamiltonian_i[j + dim * k]
                    / partitionSum;
            }
        }

        energy_i += eigenvalues_i[k];
        energy_i_squared += pow(eigenvalues_i[k], 2.0);
    }

    energy_i /= partitionSum;
    energy_i_squared /= partitionSum;

    for(A_row = 0; A_row <= spins_A; A_row++)
    {
        for(A_column = 0; A_column <= spins_A; A_column++)
        {
            for(B = 0; B <= spins_B; B++)
            {
                rho_A[A_row + dim_A * A_column] += 
                    binomial_prefactors_AB[A_row + dim_A*B] 
                    * binomial_prefactors_AB[A_column + dim_A*B] 
                    * rho_AB[(A_row+B) + dim * (A_column + B)];
            }
        }
    }

#ifdef DEBUG
    check_symmetric(rho_AB, dim);
#endif // ifdef DEBUG
    report_elapsed_time();

    magnetization = 0.0;
    magnetization_squared = 0.0;

    for(i = 0; i < dim; i++)
    {
        magnetization += (2 * i - N) * rho_AB[i + dim * i];
        magnetization_squared += pow(2 * i - N, 2.0) * rho_AB[i + dim * i];
    }

    magnetization = magnetization / (2*N);
    magnetization_squared = magnetization_squared / (4*N*N);
    magnetization_err = sqrt(magnetization_squared-magnetization*magnetization);

    start = clock();
    snprintf(buf2, sizeof(buf2), "Diagonalizing rho_AB (%lli x %lli) ", dim, dim);
    report_status(buf2);
    diagonalize_symmetric(rho_AB, (lapack_int) dim, rho_AB_eigenvalues);
    free(rho_AB);

    start = clock();
    snprintf(buf2, sizeof(buf2), "Diagonalizing rho_A (%lli x %lli) ", dim_A, dim_A);
    report_status(buf2);
    diagonalize_symmetric(rho_A, (lapack_int) dim_A, rho_A_eigenvalues);
    free(rho_A);

    S_A = calculate_vonNeumann_entropy(rho_A_eigenvalues, dim_A);
    S2_A = calculate_Renyi_entropy(rho_A_eigenvalues, dim_A, 2.0);
    Smin_A = calculate_min_entropy(rho_A_eigenvalues, dim_A);
    free(rho_A_eigenvalues);

    S_AB = calculate_vonNeumann_entropy(rho_AB_eigenvalues, dim);
    S2_AB = calculate_Renyi_entropy(rho_AB_eigenvalues, dim, 2.0);
    Smin_AB = calculate_min_entropy(rho_AB_eigenvalues, dim);
    free(rho_AB_eigenvalues);

    // We calculate the energy density after the quench to find an effective temperature
    energy_f = 0.0;
    energy_f_squared = 0.0;

    for(k = 0; k < dim; k++)
    {
        double weight = 0.0;
            
            for(i = 0; i < nStates; i++)
            {
                double inner_product_ki = 0.0;
                for(j = 0; j < dim; j++)
                {
                    inner_product_ki += hamiltonian_f[j + dim * k] * hamiltonian_i[j + dim * i];
                }

                weight += pow(inner_product_ki, 2.0);
            }

        energy_f += eigenvalues_f[k] * weight;
        energy_f_squared += pow(eigenvalues_f[k], 2.0) * weight;

    }

    energy_f /= partitionSum;
    energy_f_squared /= partitionSum;

    // double effective_beta_left = 0;
    // double effective_beta_right = beta;


    // // After a quench the effective temperature might go down,
    // // so we first find a beta large enough such that <H>_equilibrium(beta) < <H>_after_quench
    // while(1)
    // {
    //     double effective_beta_energy = 0.0;
    //     double effective_beta_partitionSum = 0.0;

    //     for(k = 0; k < dim; k++)
    //     {
    //         effective_beta_partitionSum += exp(-1.0 * effective_beta_right * N * (eigenvalues_f[k]-min_eigenvalue_f));
    //         effective_beta_energy += eigenvalues_f[k] * exp(-1.0 * effective_beta_right * N * (eigenvalues_f[k]-min_eigenvalue_f));
    //     }
    //     effective_beta_energy /= effective_beta_partitionSum;

    //     if(effective_beta_energy > energy_f)
    //     {
    //         effective_beta_left = effective_beta_right;
    //         effective_beta_right = effective_beta_right * 2.0;
    //     }
    //     else
    //     {
    //         break;
    //     }
    // }

    // // Now we do a binary search for <H>_equilibrium(beta) = <H>_after_quench
    // int effective_beta_runs = 0;
    // while( fabs(effective_beta_left - effective_beta_right) > 1E-12 || effective_beta_runs > 100)
    // {
    //     effective_beta_runs++;
    //     double effective_beta_guess = (effective_beta_left + effective_beta_right)/2.0;
    //     double effective_beta_energy = 0.0;
    //     double effective_beta_partitionSum = 0.0;

    //     for(k = 0; k < dim; k++)
    //     {
    //         effective_beta_partitionSum += exp(-1.0 * effective_beta_guess * N * (eigenvalues_f[k]-min_eigenvalue_f));
    //         effective_beta_energy += eigenvalues_f[k] * exp(-1.0 * effective_beta_guess * N * (eigenvalues_f[k]-min_eigenvalue_f));
    //     }
    //     effective_beta_energy /= effective_beta_partitionSum;
    //     if(effective_beta_energy > energy_f)
    //     {
    //         effective_beta_left = effective_beta_guess;
    //     }
    //     else
    //     {
    //         effective_beta_right = effective_beta_guess;
    //     }
    // }

    printf("\n");
    printf("======  Pre Quench  (Γ = %lg)  ====== \n", Gamma_i);
    printf("    Energy Density: %lf ± %lf (%5.2lf%% above ground state)\n", energy_i, sqrt(energy_i_squared - energy_i * energy_i),
        100 * (energy_i - min_eigenvalue_i)/(max_eigenvalue_i - min_eigenvalue_i));
    // printf("        Exact Beta: %lg\n", beta);
    printf("     Magnetization: %lf ± %lf\n", magnetization, magnetization_err);
    printf("\n");
    printf("    S_A = %g\n", S_A);
    printf("    S_AB = %g\n", S_AB);
    printf("    S2_A = %g\n", S2_A);
    printf("    S2_AB = %g\n", S2_AB);
    printf("\n");
    printf("======  Post Quench (Γ = %lg)  ====== \n", Gamma_f);
    printf("    Energy Density: %lf ± %lf (%5.2lf%% above ground state)\n", energy_f, sqrt(energy_f_squared - energy_f * energy_f),
        100 * (energy_f - min_eigenvalue_f)/(max_eigenvalue_f - min_eigenvalue_f));
    // printf("    Effective Beta: %lf\n", (effective_beta_left+effective_beta_right)/2.0);
    printf("\n");

    t = starttime;

    number_of_steps = (int) ((stoptime - starttime)/dt + 0.5);

    for(step = 0; step <= number_of_steps && dt > 0; step++)
    {
        t = starttime + step * dt;
        printf("\n======  Step %i/%i  ======\n", step, number_of_steps);
        snprintf(buf, sizeof(buf), "N%i Γi%g Γf%g nS%i t%g lf%g ", 
            N, Gamma_i, Gamma_f, nStates, t, lfi);

        if(find_results())
        {
            printf("Skipping t = %g\n", t);
            continue;
        }

        start = clock();
        report_status("Calculating |U(t)> and rho_CD");

#ifdef HAAR_RANDOM_UNITARY

        my_memset(rotated_eigenvectors, 0, dim * dim, sizeof(double _Complex));
        get_Haar_random_unitary(random_unitary, (lapack_int) dim);

#ifdef DEBUG
        check_unitary(random_unitary, dim);
#endif // ifdef DEBUG

        for(i = 0; i < dim; i++)
        {
            for(j = 0; j < dim; j++)
            {
                for(k = 0; k < dim; k++)
                {
                    rotated_eigenvectors[i + (j * dim)] += random_unitary[i + (k * dim)] 
                        * hamiltonian_f[k + (j * dim)];
                }
            }
        }

#endif // ifdef HAAR_RANDOM_UNITARY

        my_memset(tfd_state, 0, dim * dim, sizeof(double _Complex));
        my_memset(rho_CD, 0, dim * dim, sizeof(double _Complex));

        for(k = 0; k < dim; k++)
        {
            for(l = 0; l < dim; l++)
            {
                double _Complex weight_tfd = 0.0;
                double _Complex weight_rho = 0.0;
                
                for(i = 0; i < nStates; i++)
                {
                    double inner_product_ki = 0.0;
                    for(j = 0; j < dim; j++)
                    {
                        inner_product_ki += hamiltonian_f[j + dim * k] * hamiltonian_i[j + dim * i];
                    }

                    double inner_product_il = 0.0;
                    for(j = 0; j < dim; j++)
                    {
                        inner_product_il += hamiltonian_i[j + dim * i] * hamiltonian_f[j + dim * l];
                    }

                    weight_tfd += inner_product_ki * inner_product_il;
                    weight_rho += inner_product_ki * inner_product_il;
                }

#ifndef HAAR_RANDOM_UNITARY
                weight_tfd = weight_tfd * cexp(-1.0 * (double _Complex) _Complex_I * t * N * eigenvalues_f[l]);
                weight_rho = weight_rho 
                    * cexp(-1.0 * (double _Complex) _Complex_I * t * N * (eigenvalues_f[k]-eigenvalues_f[l]));
#endif // ifndef HAAR_RANDOM_UNITARY

                for(i = 0; i < dim; i++)
                {
                    for(j = 0; j < dim; j++)
                    {
#ifdef HAAR_RANDOM_UNITARY
                        tfd_state[i + dim * j] +=
                            weight_tfd
                            * hamiltonian_f[i + dim * k]
                            * rotated_eigenvectors[j + dim * l];

                        rho_CD[i + dim * j] +=
                            weight_rho
                            * rotated_eigenvectors[i + dim * k]
                            * conj(rotated_eigenvectors[j + dim * l]);
#else
                        tfd_state[i + dim * j] +=
                            weight_tfd
                            * hamiltonian_f[i + dim * k]
                            * hamiltonian_f[j + dim * l];

                        rho_CD[i + dim * j] +=
                            weight_rho
                            * hamiltonian_f[i + dim * k]
                            * hamiltonian_f[j + dim * l];
#endif
                    }
                }
            }
        }

        for(i = 0; i < dim * dim; i++)
        {
            rho_CD[i] /= partitionSum;
        }

#ifdef DEBUG
        check_hermitian(rho_CD, dim);
#endif // ifdef DEBUG

        // Magnetization of subsystem CD
        magnetization = 0.0;
        magnetization_squared = 0.0;
        for(i = 0; i < dim; i++)
        {
            magnetization = magnetization + (2 * i - N) * creal(rho_CD[i + dim * i]);
            magnetization_squared = magnetization_squared 
                + pow(2 * i - N, 2.0) * creal(rho_CD[i + dim * i]);
        }

        magnetization = magnetization / (2*N);
        magnetization_squared = magnetization_squared / (4*N*N);
        magnetization_err = sqrt(magnetization_squared - magnetization*magnetization);
        report_elapsed_time();

        
        start = clock();
        report_status("Calculating rho_C");

        my_memset(rho_C, 0, dim_C * dim_C, sizeof(double _Complex));

        for(C_column = 0; C_column <= spins_C; C_column++)
        {
            for(D = 0; D <= spins_D; D++)
            {
                for(C_row = 0; C_row <= spins_C; C_row++)
                {
                        rho_C[C_row + dim_C * C_column] += binomial_prefactors_CD[C_column + dim_C*D] 
                            * binomial_prefactors_CD[C_row + dim_C*D]
                            * rho_CD[(C_row+D) + dim * (C_column+D)];
                }
            }
        }

        #ifdef DEBUG
            check_hermitian(rho_C, dim_C);
        #endif // ifdef DEBUG

        printf("\nMagnetization Distribution %lf\n[", t);
        // Magnetization of subsystem C
        double magnetization_C_sum = 0.0;

        for(i = 0; i < dim_C; i++)
        {
            double s = (2 * i - N/2);
            magnetization_C_sum += s * creal(rho_C[i + dim_C * i]);
            printf("%lf", creal(rho_C[i + dim_C * i]));
            if(i+1 < dim_C)
            {
                printf(", ");
            }
        }
        printf("],\n");

        magnetization_C_mean = magnetization_C_sum / N;

        double magnetization_C_first_central_moment = 0.0;
        double magnetization_C_second_central_moment = 0.0;
        double magnetization_C_third_central_moment = 0.0;
        double magnetization_C_fourth_central_moment = 0.0;

        for(i = 0; i < dim_C; i++)
        {
            double s = (2 * i - N/2) - magnetization_C_sum;

            magnetization_C_first_central_moment += pow(s, 1.0) * creal(rho_C[i + dim_C * i]);
            magnetization_C_second_central_moment += pow(s, 2.0) * creal(rho_C[i + dim_C * i]);
            magnetization_C_third_central_moment += pow(s, 3.0) * creal(rho_C[i + dim_C * i]);
            magnetization_C_fourth_central_moment += pow(s, 4.0) * creal(rho_C[i + dim_C * i]);

        }

        magnetization_C_variance = magnetization_C_second_central_moment/(N*N);
        magnetization_C_skewness = magnetization_C_third_central_moment
            / pow(magnetization_C_second_central_moment, 1.5);
        magnetization_C_kurtosis = magnetization_C_fourth_central_moment
            / pow(magnetization_C_second_central_moment, 2.0);

        snprintf(buf2, sizeof(buf2), "Diagonalizing rho_C (%lli x %lli) ", dim_C, dim_C);
        report_status(buf2);
        diagonalize_hermitian(rho_C, (lapack_int) dim_C, rho_C_eigenvalues);

        S_C = calculate_vonNeumann_entropy(rho_C_eigenvalues, dim_C);
        S2_C = calculate_Renyi_entropy(rho_C_eigenvalues, dim_C, 2.0);
        Smin_C = calculate_min_entropy(rho_C_eigenvalues, dim_C);

        report_elapsed_time();


        snprintf(buf2, sizeof(buf2), "Diagonalizing rho_CD (%lli x %lli) ", dim, dim);
        report_status(buf2);
        diagonalize_hermitian(rho_CD, (lapack_int) dim, rho_CD_eigenvalues);

        double S_CD = calculate_vonNeumann_entropy(rho_CD_eigenvalues, dim);
        double S2_CD = calculate_Renyi_entropy(rho_CD_eigenvalues, dim, 2.0);
        double Smin_CD = calculate_min_entropy(rho_CD_eigenvalues, dim);

        if(fabs(S_AB - S_CD) > 1E-10)
        {
            printf("    S_CD = %g\n", S_CD);
            fprintf(stderr, "\nError: S_AB != S_CD!\n");
            exit(EX_SOFTWARE);
        }

        if(fabs(S2_AB - S2_CD) > 1E-10)
        {
            printf("    S2_CD = %g\n", S2_CD);
            fprintf(stderr, "\nError: S2_AB != S2_CD!\n");
            exit(EX_SOFTWARE);
        }

        if(fabs(Smin_AB - Smin_CD) > 1E-10)
        {
            printf("    Smin_CD = %g\n", Smin_CD);
            fprintf(stderr, "\nError: Smin_AB != Smin_CD!\n");
            exit(EX_SOFTWARE);
        }


        start = clock();
        report_status("Calculating rho_AC");
        my_memset(rho_AC, 0, dim_A * dim_C * dim_A * dim_C, sizeof(double _Complex));

        for(A_column = 0; A_column <= spins_A; A_column++)
        {
            for(B = 0; B <= spins_B; B++)
            {
                for(C_column = 0; C_column <= spins_C; C_column++)
                {
                    for(D = 0; D <= spins_D; D++)
                    {
                        double _Complex bra = binomial_prefactors_AB[A_column + dim_A*B] 
                            * binomial_prefactors_CD[C_column + dim_C*D] 
                            * conj(tfd_state[(A_column+B) + dim * (C_column + D)]);
                        for(C_row = 0; C_row <= spins_C; C_row++)
                        {
                            for(A_row = 0; A_row <= spins_A; A_row++)
                            {
                                long long idx = A_row + dim_A * (C_row + dim_C 
                                * (A_column + dim_A * C_column));
                                double _Complex ket = binomial_prefactors_CD[C_row + dim_C*D] 
                                    * binomial_prefactors_AB[A_row + dim_A*B] 
                                    * tfd_state[(A_row+B) + dim * (C_row + D)];
                                rho_AC[idx] += ket * bra;
                            }
                        }
                    }
                }
            }
            snprintf(buf2, sizeof(buf2), "Calculating rho_AC %lli/%lli", A_column+1LL, dim_A);
            report_status(buf2);
        }

        for(i = 0; i < dim_A * dim_C * dim_A * dim_C; i++)
        {
            rho_AC[i] /= partitionSum;
        }

        report_elapsed_time();


#ifdef DEBUG
        check_hermitian(rho_AC, dim_A * dim_C);
#endif // ifdef DEBUG

        snprintf(buf2, sizeof(buf2), "Diagonalizing rho_AC (%lli x %lli) ", dim_A * dim_C, dim_A * dim_C);
        report_status(buf2);
        diagonalize_hermitian(rho_AC, (lapack_int) dim_A * (lapack_int) dim_C, rho_AC_eigenvalues);

        S_AC = calculate_vonNeumann_entropy(rho_AC_eigenvalues, dim_A * dim_C);
        S2_AC = calculate_Renyi_entropy(rho_AC_eigenvalues, dim_A * dim_C, 2.0);
        Smin_AC = calculate_min_entropy(rho_AC_eigenvalues, dim_A * dim_C);
        
        printf("\n");
        printf("    Magnetization    CD: %lf ± %lf\n", magnetization, magnetization_err);
        printf("    Magnetization_sq CD: %lf\n", magnetization_squared);
        printf("    (<m^2>)^0.5      CD: %lf\n", sqrt(magnetization_squared));
        printf("\n");
        printf("    Magnetization C         : %lf ± %lf\n", 
            magnetization_C_mean, sqrt(magnetization_C_variance));
        printf("    Magnetization C Skewness: %lf\n", magnetization_C_skewness);
        printf("    Magnetization C Kurtosis: %lf\n", magnetization_C_kurtosis);

        printf("    Magnetization C m_1: %lf\n", magnetization_C_first_central_moment);
        printf("    Magnetization C m_2: %lf\n", magnetization_C_second_central_moment);
        printf("    Magnetization C m_3: %lf\n", magnetization_C_third_central_moment);
        printf("    Magnetization C m_4: %lf\n", magnetization_C_fourth_central_moment);

        // printf("    (<m^2>)^0.5       C: %lf\n", sqrt(magnetization_C_squared));
        printf("\n");
        printf("    S_AC = %g\n", S_AC);
        printf("    S2_AC = %g\n", S2_AC);
        printf("    S_C = %g\n", S_C);
        printf("    S2_C = %g\n", S2_C);
        printf("    I_3 = %g\t(= %g * log( (N/2+1)^2 )\n", 2 * (S_A+S_C) - (S_AB + 2 * S_AC), 
            (2 * (S_A + S_C) - (S_AB + 2*S_AC))*0.5/log2(N/2+1));
        printf("    I_3^(2) = %g\t(= %g * log( (N/2+1)^2 )\n", 2 * (S2_A + S2_C) - (S2_AB + 2 * S2_AC), 
            (2 * (S2_A + S2_C) - (S2_AB + 2* S2_AC))*0.5/log2(N/2+1));

        store_results();
    }

#ifdef USE_INTEL_MKL
    mkl_free_buffers();
    mkl_finalize();
#endif // ifdef USE_INTEL_MKL

    free(rho_AC_eigenvalues);
    free(rho_AC);
    free(tfd_state);
    free(eigenvalues_f);
    free(hamiltonian_f);
    free(eigenvalues_i);
    free(hamiltonian_i);
    free(binomial_prefactors_CD);
    free(binomial_prefactors_AB);
    
    start = init;

    printf("\n");
    report_elapsed_time();
    return 0;
}


int find_results(void)
{
    // Sqlite3 
    sqlite3 *db;
    int rc;
    char *zErrMsg = 0;
    sqlite3_stmt *stmt;
    int count;

    rc = sqlite3_open(DBPATH, &db);
    if(rc) {
        fprintf(stderr, "\nError: Can't open database in %s:%i: %s\n", __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    sqlite3_busy_timeout(db, 100000);

    rc = sqlite3_exec(db, 
        "CREATE TABLE IF NOT EXISTS " TABLENAME " "
        "(N INTEGER, gamma_i REAL, gamma_f REAL, nStates INTEGER, "
        "t REAL, lfi REAL, magnetization REAL, magnetization_err REAL, "
        "magnetization_C_mean REAL, magnetization_C_variance REAL, "
        "magnetization_C_skewness REAL, magnetization_C_kurtosis REAL, "
        "S_A REAL, S_AB REAL, S_AC REAL, S_C REAL, "
        "S2_A REAL, S2_AB REAL, S2_AC REAL, S2_C REAL, "
        "Smin_A REAL, Smin_AB REAL, Smin_AC REAL, Smin_C REAL, "
        "version TEXT,"
        "CONSTRAINT " TABLENAME "_uniq UNIQUE "
        "(N, gamma_i, gamma_f, nStates, t, lfi))", NULL, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, zErrMsg);
        sqlite3_free(zErrMsg);
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_prepare_v2(db, "SELECT COUNT(*) FROM " TABLENAME " WHERE N = ?1 AND gamma_i = ?2 "
        "AND gamma_f = ?3 AND nStates = ?4 AND t = ?5 AND lfi = ?6;", -1, &stmt, NULL);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_int(stmt, 1, N);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 2, Gamma_i);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 3, Gamma_f);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_int(stmt, 4, nStates);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 5, t);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 6, lfi);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_step(stmt);
    if(rc != SQLITE_ROW)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }
    
    count = sqlite3_column_int(stmt, 0);

    rc = sqlite3_reset(stmt);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_finalize(stmt);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_close(db);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    return count > 0;
}


void store_results(void)
{
    // Sqlite3 
    sqlite3 *db;
    int rc;
    char *zErrMsg = 0;
    sqlite3_stmt *stmt;

    rc = sqlite3_open(DBPATH, &db);
    if(rc) {
        fprintf(stderr, "\nError: Can't open database in %s:%i: %s\n", __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    sqlite3_busy_timeout(db, 100000);

    rc = sqlite3_exec(db, 
        "CREATE TABLE IF NOT EXISTS " TABLENAME " "
        "(N INTEGER, gamma_i REAL, gamma_f REAL, nStates INTEGER, "
        "t REAL, lfi REAL, magnetization REAL, magnetization_err REAL, "
        "magnetization_C_mean REAL, magnetization_C_variance REAL, "
        "magnetization_C_skewness REAL, magnetization_C_kurtosis REAL, "
        "S_A REAL, S_AB REAL, S_AC REAL, S_C REAL, "
        "S2_A REAL, S2_AB REAL, S2_AC REAL, S2_C REAL, "
        "Smin_A REAL, Smin_AB REAL, Smin_AC REAL, Smin_C REAL, "
        "version TEXT,"
        "CONSTRAINT " TABLENAME "_uniq UNIQUE "
        "(N, gamma_i, gamma_f, nStates, t, lfi))", NULL, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, zErrMsg);
        sqlite3_free(zErrMsg);
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_prepare_v2(db, 
        "INSERT INTO " TABLENAME " (N, gamma_i, gamma_f, nStates, t, lfi, "
        "magnetization, magnetization_err, magnetization_C_mean, magnetization_C_variance, "
        "magnetization_C_skewness, magnetization_C_kurtosis, "
        "S_A, S_AB, S_AC, S_C, S2_A, S2_AB, S2_AC, S2_C, "
        "Smin_A, Smin_AB, Smin_AC, Smin_C, version) "
        "VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, "
        "?16, ?17, ?18, ?19, ?20, ?21, ?22, ?23, ?24, ?25);", 
        -1, &stmt, NULL);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, zErrMsg);
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_int(stmt, 1, N);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 2, Gamma_i);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 3, Gamma_f);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_int(stmt, 4, nStates);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 5, t);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 6, lfi);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 7, magnetization);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 8, magnetization_err);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 9, magnetization_C_mean);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 10, magnetization_C_variance);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 11, magnetization_C_skewness);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 12, magnetization_C_kurtosis);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 13, S_A);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 14, S_AB);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 15, S_AC);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 16, S_C);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 17, S2_A);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 18, S2_AB);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 19, S2_AC);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 20, S2_C);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 21, Smin_A);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 22, Smin_AB);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 23, Smin_AC);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 24, Smin_C);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_text(stmt, 25, GIT_DESCRIBE, sizeof(GIT_DESCRIBE), SQLITE_STATIC);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_step(stmt);
    if(rc != SQLITE_OK && rc != SQLITE_DONE) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        if(rc == SQLITE_CONSTRAINT)
        {
            sqlite3_finalize(stmt);
            sqlite3_close(db);
            return;
        }
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_reset(stmt);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_finalize(stmt);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_close(db);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }
}
