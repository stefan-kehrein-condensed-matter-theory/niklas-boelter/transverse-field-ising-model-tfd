#!/usr/bin/env python3
import subprocess, sys, io, os, logging, shlex, time, datetime, math, select

nproc = 0

def lines(filepath):
    global nproc
    times = []
    lines = []
    with open(filepath) as fp:
        for l in fp.readlines():
            l = l.strip()
            if len(l) > 0:
                lines.append(l)

    start = datetime.datetime.now()

    i = 0
    total = len(lines)
    for line in lines:
        i = i + 1
        finished = max(i - nproc, 0)
        elapsed = datetime.datetime.now() - start
        remaining = "??"
        
        if finished != 0:
            remaining = elapsed * (total - finished)/finished
            remaining = datetime.timedelta(days=remaining.days, seconds=remaining.seconds, microseconds=0)
            times.append(datetime.datetime.now())

        # Be a bit "smarter" about the remaining time!
        if len(times) > 4 * nproc + 1:
            remaining = (times[-1] - times[-4 * nproc - 1]) * (total - finished) / (4 * nproc)
            remaining = datetime.timedelta(days=remaining.days, seconds=remaining.seconds, microseconds=0)

        elapsed = datetime.timedelta(days=elapsed.days, seconds=elapsed.seconds, microseconds=0)

        print(("\r\033[97m{0:0" + str(math.ceil(math.log(total+1)/math.log(10))) + "}\033[00m/{1} done, \033[34m{2}\033[00m elapsed, \033[31m{3}\033[00m remaining").format(finished, total, elapsed, remaining), end='')
        yield line.strip()

def removeFinishedProcesses(processes):
    """ given a list of (commandString, process), 
        remove those that have completed and return the result 
    """
    newProcs = []
    for pollCmd, pollProc in processes:
        retCode = pollProc.poll()
        if retCode==None:
            # still running
            newProcs.append((pollCmd, pollProc))
        elif retCode!=0:
            # failed
            print("\nCommand %s failed!\n" % pollCmd)
        else:
            logging.info("Command %s completed successfully" % pollCmd)
    return newProcs

def runCommands(commands):
    global nproc

    pause = 0
    quit = 0
    skipped_commands = 0
    total_commands = 0
    processes = []
    for command in commands:
        total_commands = total_commands + 1
        if quit == 1:
            skipped_commands = skipped_commands + 1
            continue

        logging.info("Starting process %s" % command)
        try:
            proc =  subprocess.Popen(shlex.split(command), stdout=subprocess.DEVNULL)
            procTuple = (command, proc)
            processes.append(procTuple)
        except Exception as e:
            print()
            print(e)
            print()

        while len(processes) >= nproc or pause == 1:
            if len(processes) > 0:
                os.wait()
            else:
                time.sleep(0.1)

            processes = removeFinishedProcesses(processes)

            while select.select([sys.stdin,],[],[],0.0)[0]:
                l = sys.stdin.readline().strip()
                t = l.split(' ')
                if l == 'pause':
                    pause = 1 - pause
                    print()
                    print("Toggling Pause Mode")
                    print()
                if l == 'quit':
                    quit = 1
                    pause = 0
                if t[0] == 'nproc':
                    nproc = int(t[1])
                    print()
                    print("Set number of processes to " + str(nproc))
                    print()
    if quit == 1:
        print()
        print()
        print('\033[34mFinished ' + str(total_commands - skipped_commands) + ' commands\033[00m')
        print('\033[31mSkipped ' + str(skipped_commands) + ' commands\033[00m')
        print()

    # wait for all processes
    while len(processes)>0:
        print("\rWaiting for processes to finish: {0} left\033[K".format(len(processes)), end='')
        if os.name == 'posix':
            os.wait()
        else:
            time.sleep(0.5)
        processes = removeFinishedProcesses(processes)
    print("\rDone\033[K")

    logging.info("All processes completed")

def main():
    global nproc
    # logging.getLogger().setLevel(logging.DEBUG)
    if(len(sys.argv) != 3):
        print("{0} <n_processes> <file path>\nAccepts a list of commands from <file path> and executes n_processes of them in parallel.".format(sys.argv[0]))
        return

    nproc = int(sys.argv[1])
    start = datetime.datetime.now()
    runCommands(lines(sys.argv[2])) 
    print("Total time taken: {0}".format(datetime.datetime.now()-start))
if __name__ == '__main__':
    main()

