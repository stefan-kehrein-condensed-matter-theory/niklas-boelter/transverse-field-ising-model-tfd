/*  Copyright 2019 Niklas Bölter <niklas.boelter@theorie.physik.uni-goettingen.de> */

#ifndef FERMION_CHAIN_UTIL_H
#define FERMION_CHAIN_UTIL_H

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <string.h>
#include <sysexits.h>
#include <time.h>
#include <limits.h>

#ifdef USE_INTEL_MKL
#define MKL_Complex16 double _Complex
#include <mkl.h>
#else
#include <lapacke.h>
#endif

#ifndef lapack_int
#define lapack_int MKL_INT
#endif

extern clock_t start;
extern int argv_len; // Used for status reporting
extern char *argv_ptr;
extern char *status_prefix; // Prefix string for status reporting

void * my_calloc(long long count, size_t size);
void * my_memset(void *b, int c, long long count, size_t size);

void show_rho_eigenvalues(const double *rho_eigenvalues, long long dim);
double calculate_Renyi_entropy(const double *eigenvalues, long long dim, double alpha);
double calculate_vonNeumann_entropy(const double *eigenvalues, long long dim);
double calculate_min_entropy(const double *eigenvalues, long long dim);

void diagonalize(double _Complex *M, lapack_int dim, double _Complex *eigenvalues);
void diagonalize_hermitian_eigenvectors(double _Complex *M, lapack_int dim, double *eigenvalues);
void diagonalize_hermitian(double _Complex *M, lapack_int dim, double *eigenvalues);
void diagonalize_symmetric_eigenvectors(double *M, lapack_int dim, double *eigenvalues);
void diagonalize_symmetric(double *M, lapack_int dim, double *eigenvalues);

void report_status(const char *str);
void report_elapsed_time(void);

void check_normalized(const double *V, long long N);
void check_normalized_complex(const double _Complex *V, long long N);
void check_unitary(const double _Complex *U, long long N);
void check_hermitian(const double _Complex *rho, long long N);
void check_symmetric(const double *rho, long long N);
void show_checksum(const void *data, const size_t size);

double binomial_prefactor(int spins_A, int spins_B, int spinsUp_A, int spinsUp_B);

inline long long min(long long a, long long b) {
    if (a < b)
    {
        return a;
    }
    return b;
}

inline long long max(long long a, long long b) {
    if (a > b)
    {
        return a;
    }
    return b;
}

/*
 * Code Taken from
 * Henry S. Warren, “Hacker’s Delight”, Addison-Wesley Professional, 2013, Second Edition 
 */

inline uint32_t fallback_pext_u32(uint32_t value, uint32_t mask)
{
    int i;
    uint32_t mk = (~mask << 1);                    // we will count 0's to the right
    uint32_t mp;
    uint32_t mv;
    uint32_t tx;
    value = (value & mask);                         // clear irrelevant bits

    for (i = 0; i < 5; ++i)                         // log_2 of the bit size (here, 32 bits)
    {
        mp     = mk ^ (mk <<  1);                   // parallel suffix
        mp     = mp ^ (mp <<  2);
        mp     = mp ^ (mp <<  4);
        mp     = mp ^ (mp <<  8);
        mp     = mp ^ (mp << 16);
        mv     = (mp & mask);                       // bits to move
        mask   = ((mask ^ mv) | (mv >> (1 << i)));  // compress mask
        tx      = (value & mv);
        value = ((value ^ tx) | (tx >> (1 << i)));    // compress value
        mk    &= ~mp;
    }
    return value;
}


inline uint32_t fallback_pdep_u32(uint32_t value, uint32_t mask)
{
   uint32_t m0, mk, mp, mv, tx;
   uint32_t array[5];
   int i;

   m0 = mask;              // Save original mask.
   mk = ~mask << 1;        // We will count 0's to right.

   for (i = 0; i < 5; i++) {
      mp = mk ^ (mk << 1);              // Parallel suffix.
      mp = mp ^ (mp << 2);
      mp = mp ^ (mp << 4);
      mp = mp ^ (mp << 8);
      mp = mp ^ (mp << 16);
      mv = mp & mask;                      // Bits to move.
      array[i] = mv;
      mask = (mask ^ mv) | (mv >> (1 << i));  // Compress m.
      mk = mk & ~mp;
   }

   for (i = 4; i >= 0; i--) {
      mv = array[i];
      tx = value << (1 << i);
      value = (value & ~mv) | (tx & mv);
//    value = ((value ^ t) & mv) ^ value;           // Alternative for above line.
   }
   return value & m0;       // Clear out extraneous bits.
}


inline uint64_t fallback_pext_u64(uint64_t value, uint64_t mask)
{
    int i;
    uint64_t mk = (~mask << 1);                    // we will count 0's to the right
    uint64_t mp;
    uint64_t mv;
    uint64_t tx;
    value = (value & mask);                         // clear irrelevant bits

    for (i = 0; i < 6; ++i)                         // log_2 of the bit size (here, 32 bits)
    {
        mp     = mk ^ (mk <<  1);                   // parallel suffix
        mp     = mp ^ (mp <<  2);
        mp     = mp ^ (mp <<  4);
        mp     = mp ^ (mp <<  8);
        mp     = mp ^ (mp << 16);
        mp     = mp ^ (mp << 32);
        mv     = (mp & mask);                       // bits to move
        mask   = ((mask ^ mv) | (mv >> (1 << i)));  // compress mask
        tx      = (value & mv);
        value = ((value ^ tx) | (tx >> (1 << i)));    // compress value
        mk    &= ~mp;
    }
    return value;
}


inline uint64_t fallback_pdep_u64(uint64_t value, uint64_t mask)
{
   uint64_t m0, mk, mp, mv, tx;
   uint64_t array[6];
   int i;

   m0 = mask;              // Save original mask.
   mk = ~mask << 1;        // We will count 0's to right.

   for (i = 0; i < 6; i++) {
      mp = mk ^ (mk << 1);              // Parallel suffix.
      mp = mp ^ (mp << 2);
      mp = mp ^ (mp << 4);
      mp = mp ^ (mp << 8);
      mp = mp ^ (mp << 16);
      mp = mp ^ (mp << 32);
      mv = mp & mask;                      // Bits to move.
      array[i] = mv;
      mask = (mask ^ mv) | (mv >> (1 << i));  // Compress m.
      mk = mk & ~mp;
   }

   for (i = 5; i >= 0; i--) {
      mv = array[i];
      tx = value << (1 << i);
      value = (value & ~mv) | (tx & mv);
//    value = ((value ^ t) & mv) ^ value;           // Alternative for above line.
   }
   return value & m0;       // Clear out extraneous bits.
}

#endif

