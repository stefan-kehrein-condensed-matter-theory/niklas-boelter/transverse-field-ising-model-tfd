## Install
To build, you need the following libraries: BSD, GMP, Lapack, Lapacke

On Arch Linux derivatives you should only need

`$ sudo pacman -S lapacke base-devel`

On Debian based systems

`$ sudo apt install liblapacke-dev liblapack-dev libgmp-dev libbsd-dev build-essential`

There are multiple options in the Makefiles directory, you can try the basic one:

`$ ln -s Makefiles/linux-gcc.Makefile Makefile`

`$ make`

## Usage
Run the executables to get a helpful usage message:

`$ ./transverse-field-ising-model-tfd`

